﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CssFilesCollector
{
    public class UrisFinder
    {
        private int urisToExtract = 10; // Default count uris for extracting

        public UrisFinder() { }

        public UrisFinder(int urisCountToExctract)
        {
            this.urisToExtract = urisCountToExctract;
        }

        public IEnumerable<Uri> GetUris(string Source)
        {
            int count = 0;

            using (StringReader sr = new StringReader(Source))
            {
                string readed = string.Empty;

                while ((readed = sr.ReadLine()) != null && count < urisToExtract)
                {
                    int commaPosition = readed.IndexOf(','); // Delimiter in CSV file

                    if (commaPosition > -1 && readed.Length > commaPosition)
                    {
                        yield return new Uri("http://" + readed.Substring(commaPosition + 1));
                        count++;
                    }
                }
            }
        }
    }
}
