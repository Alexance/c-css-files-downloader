﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CssFilesCollector
{
    public class DownloadedFile
    {
        public Uri @Uri { get; private set; }
        public byte[] Content { get; private set; }

        public DownloadedFile(Uri Uri, byte[] Content)
        {
            this.Uri = Uri;
            this.Content = Content;
        }

        public string GetFileName()
        {
            return Path.GetFileName(this.Uri.ToString());
        }
    }
}
