﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace CssFilesCollector
{
    public class FilesSaver
    {
        private DirectoryInfo root = null;
        private int partOfPath;

        public int PartLength
        {
            get { return partOfPath; }
            set { this.partOfPath = value;  }
        }

        /// <summary>
        /// Creates saver for files
        /// </summary>
        /// <param name="AbsoluteDirectory">Absolute path for files saving. If directory are not exists, it will be create</param>
        /// <param name="PartLength">Sets the length of part for saving sites</param>
        public FilesSaver(string AbsoluteDirectory, int PartLength)
        {
            this.partOfPath = PartLength;

            if (!Directory.Exists(AbsoluteDirectory))
            {
                try
                {
                    root = Directory.CreateDirectory(AbsoluteDirectory);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                root = new DirectoryInfo(AbsoluteDirectory);
            }  
        }

        public void DownloadAndSaveFileAsync(KeyValuePair<Uri, Uri> SiteAndCssUrl)
        {
            string filename = this.GetFilenameForSave(SiteAndCssUrl.Key);

            this.DownloadFile(SiteAndCssUrl.Value, filename);
        }

        public void SaveToFileAsync(KeyValuePair<Uri, string> SiteAndCssCode)
        {
            string filename = this.GetFilenameForSave(SiteAndCssCode.Key, @"page_{0}.css");

            File.WriteAllText(filename, SiteAndCssCode.Value);
        }

        private string GetFilenameForSave(Uri url, string template = @"style_{0}.css")
        {
            string subPath = this.GetSubPath(url);

            DirectoryInfo catalogDirectory = root.CreateSubdirectory(subPath); // http://google.com/ => Goog (if PartLength == 4), etc.
            DirectoryInfo siteDirectory = catalogDirectory.CreateSubdirectory(url.Host);

            int currentFile = siteDirectory.GetFiles().Length;

            string fileName = Path.Combine(siteDirectory.ToString(), template);
            return string.Format(fileName, currentFile);
        }

        private void DownloadFile(Uri uri, string fileName)
        {
            using (TimeoutWebClient twc = new TimeoutWebClient(10000))
            {
                twc.Credentials = CredentialCache.DefaultCredentials;
                twc.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

                try
                {
                    twc.DownloadFileAsync(uri, fileName);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private string GetSubPath(Uri uri)
        {
            string hostNameOnly = this.FormatUriWithoutZone(uri);

            if (hostNameOnly.Length < partOfPath)
                return hostNameOnly;

            return hostNameOnly.Substring(0, partOfPath);
        }

        private string FormatUriWithoutZone(Uri uri)
        {
            return uri.Host.Remove(uri.Host.IndexOf('.'));
        }
    }
}
