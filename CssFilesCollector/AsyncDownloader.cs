﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;

using System.IO;


// http://w3ka.blogspot.com/2009/12/how-to-fix-webclient-timeout-issue.html
// This feature for new timeout in WebClient!!!
namespace CssFilesCollector
{
    public class AsyncDownloader
    {
        private int asyncTasksCount = 10;
        private bool downloadAsCompressed = false;

        public AsyncDownloader(int asyncTasksCount, bool downloadAsCompressed)
        {
            this.asyncTasksCount = asyncTasksCount;
            this.downloadAsCompressed = downloadAsCompressed;
        }

        public IEnumerable<KeyValuePair<Uri, string>> DownloadPages(List<Uri> uris)
        {
            Func<Uri, Task<KeyValuePair<Uri, string>>> downloadFunction = (uri) =>
            {
                return Task<KeyValuePair<Uri, string>>.Factory.StartNew((uriToDownload) =>
                {
                    Uri toDownload = (Uri)uriToDownload;
                    KeyValuePair<Uri, string> res = new KeyValuePair<Uri, string>(toDownload, DownloadString(toDownload));

                    return res;
                }, uri);
            };

            foreach (var downloadedPage in this.Download<KeyValuePair<Uri, string>>(uris, downloadFunction))
            {
                yield return downloadedPage;
            }
        }

        public IEnumerable<DownloadedFile> DownloadFiles(List<Uri> uris)
        {
            Func<Uri, Task<DownloadedFile>> downloadFunction = (uri) =>
            {
                return Task<DownloadedFile>.Factory.StartNew((uriToDownload) =>
                    {
                        Uri toDownload = (Uri) uriToDownload;
                        return new DownloadedFile(toDownload, DownloadFile(toDownload));
                    }, uri);
            };

            foreach (var downloadedFile in this.Download<DownloadedFile>(uris, downloadFunction))
            {
                yield return downloadedFile;
            }
        }

        private IEnumerable<T> Download<T>(List<Uri> uris, Func<Uri, Task<T>> downloadFunction)
        {
            var queries = uris.Select(uri => downloadFunction(uri));

            Task<T>[] current = queries.Take(asyncTasksCount).ToArray();
            Queue<Task<T>> waiting = new Queue<Task<T>>(queries.Skip(asyncTasksCount));

            int endedTaskNumber = 0;
            while (current.Length > 0)
            {
                endedTaskNumber = Task.WaitAny(current);
                yield return current[endedTaskNumber].Result;
                current = current.SwapOnRunningOrRemove(endedTaskNumber, waiting);
            }
        }

        private byte[] DownloadFile(Uri uri)
        {
            using (WebClient wc = new WebClient())
            {
                try
                {
                    return wc.DownloadData(uri);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return new byte[0];
                }
            }
        }

        private string DownloadString(Uri uri)
        {
            using (WebClient wc = new WebClient())
            {
                if (downloadAsCompressed)
                {
                    wc.Headers.Add("Accept-Encoding", "gzip,deflate");
                }

                try
                {
                    return wc.DownloadString(uri);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return string.Empty;
                }                
            }
        }
    }

    public static class HelperFunctions
    {
        public static T[] SwapOnRunningOrRemove<T>(this T[] array, int index, Queue<T> waitingTasks)
        {
            if (array.Length > index && index > -1)
            {
                if (waitingTasks.Count > 0)
                    array[index] = waitingTasks.Dequeue();
                else
                    array = array.RemoveAt(index);
            }

            return array;
        }

        public static T[] RemoveAt<T>(this T[] array, int index)
        {
            if (array.Length <= index || index < -1)
            {
                throw new ArgumentOutOfRangeException("Index out of range");
            }

            T[] newArray = new T[array.Length - 1];

            if (index > 0)
                Array.Copy(array, 0, newArray, 0, index);

            if (index < array.Length - 1)
                Array.Copy(array, index + 1, newArray, index, array.Length - index - 1);

            return newArray;
        }
    }
}
