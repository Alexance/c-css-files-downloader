﻿using System;
using System.Net;

namespace CssFilesCollector
{
    class TimeoutWebClient : WebClient
    {
        private int timeout;
        public int Timeout
        {
            get
            {
                return timeout;
            }
            set
            {
                this.timeout = value;
            }
        }

        /// <summary>
        /// Creates TimeoutWebClient with custom timeout
        /// </summary>
        /// <param name="Milliseconds">Milliseconds for timeout</param>
        public TimeoutWebClient(int Milliseconds)
        {
            this.timeout = Milliseconds;
        }

        /// <summary>
        /// Creates TimeoutWebClient with standart 60 seconds timeout
        /// </summary>
        public TimeoutWebClient()
        {
            this.timeout = 60000;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var result = (HttpWebRequest) base.GetWebRequest(address);
            result.Timeout = this.timeout;
            result.ReadWriteTimeout = this.timeout;

            return result;
        }
    }
}
