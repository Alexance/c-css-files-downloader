﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;

using HtmlParser.Lexer;
using HtmlParser.Hash;

namespace CssFilesCollector
{
    public class LinkExtractor
    {
        public LinkExtractor() { }

        public IEnumerable<KeyValuePair<Uri, Uri>> GetLinks(Uri siteUri, string page)
        {
            // Needed parts:
            // 1 -> OpenTag Link
            // 2 -> Rel = "stylesheet"
            // 3 -> Href attribute exist
            // 4 -> End of this tag or open new

            HtmlLexer2 lexer = new HtmlLexer2();
            lexer.Load(page);

            HtmlToken link = default(HtmlToken);
            int parts = 0;

            foreach (HtmlToken token in lexer.Parse())
            {
                switch (token.TokenType)
                {
                    case TokenType.OpenTag:
                    {
                        parts = 0;

                        if (isLinkTagOpened(token))
                            parts++;
                        break;
                    }
                    case TokenType.Attribute:
                    {
                        if (isRelAttributeCorrect(token))
                        {
                            parts++;
                            continue;
                        }

                        if (isHrefAttributeCorrect(token))
                        {
                            parts++;
                            link = token;
                        }
                        break;
                    }
                }

                if (parts == 3) // Needed count for return
                {
                    // Tries create uri from string and return empty
                    // if error or correct uri
                    Uri temp = TryCreateUri(siteUri, link.GetValue());
                    if (temp != null)
                    {
                        KeyValuePair<Uri, Uri> res = new KeyValuePair<Uri, Uri>(siteUri, temp);
                        yield return res;
                    }

                    parts = 0;
                }
            }
        }

        private bool isLinkTagOpened(HtmlToken token)
        {
            return token.TokenType == TokenType.OpenTag &&
                token.GetTag() == HtmlTag.Link;
        }

        private bool isRelAttributeCorrect(HtmlToken token)
        {
            return token.TokenType == TokenType.Attribute &&
                token.GetAttribute() == HtmlAttribute.Rel &&
                string.Compare(token.GetValue(), "stylesheet", true) == 0;
        }

        private bool isHrefAttributeCorrect(HtmlToken token)
        {
            return token.TokenType == TokenType.Attribute &&
                token.GetAttribute() == HtmlAttribute.Href &&
                token.GetValue().Length > 0;
        }

        //public IEnumerable<KeyValuePair<Uri, Uri>> GetLinks(Uri siteUrl, string page)
        //{
        //    HtmlLexer2 lexer = new HtmlLexer2();
        //    lexer.Load(page);

        //    bool isLinkTagOpened = false, mayBeReturned = false;
        //    string buffer = null; // Link to CSS file for store
                        
        //    foreach (HtmlToken token in lexer.Parse())
        //    {
        //        if (token.TokenType == TokenType.OpenTag && token.GetTag() == HtmlTag.Link)
        //        {
        //            isLinkTagOpened = true;
        //            mayBeReturned = false;
        //            buffer = null;
        //            continue;
        //        }

        //        if (token.TokenType == TokenType.Attribute)
        //        {
        //            var attr = token.GetAttribute();
        //            var val = token.GetValue();

        //            if (attr == HtmlAttribute.Rel && val == "stylesheet")
        //            {
        //                mayBeReturned = true;
        //            }

        //            if (attr == HtmlAttribute.Href)
        //            {
        //                buffer = val;
        //            }
        //        }

        //        if (isLinkTagOpened && mayBeReturned && buffer != null)
        //        {
        //            // Tries create uri from string and return empty
        //            // if error or correct uri
        //            Uri temp = TryCreateUri(siteUrl, buffer);
        //            if (temp != null)
        //            {
        //                KeyValuePair<Uri, Uri> res = new KeyValuePair<Uri, Uri>(siteUrl, temp);
        //                yield return res;
        //            }

        //            // CSS link was parsed, search for new
        //            isLinkTagOpened = mayBeReturned = false;
        //            buffer = null;
        //        }

        //        if (token.TokenType == TokenType.CloseTag || token.TokenType == TokenType.OpenTag)
        //        {
        //            mayBeReturned = false;
        //            buffer = null;
        //        }
        //    }
        //}

        public IEnumerable<KeyValuePair<Uri, string>> GetCssFromPage(Uri uri, string page)
        {
            bool canReturn = false;
            HtmlLexer2 lexer = new HtmlLexer2();
            lexer.Load(page);

            foreach (HtmlToken token in lexer.Parse())
            {
                if (token.TokenType == TokenType.OpenTag && token.GetTag() == HtmlTag.Style)
                {
                    canReturn = true;
                    continue;
                }

                if (canReturn && token.TokenType == TokenType.Style)
                {
                    yield return new KeyValuePair<Uri, string>(uri, token.GetValue());
                    canReturn = false;
                }
            }
        }

        private Uri TryCreateUri(Uri absoluteName, string uri)
        {
            Uri created = null;

            if (Uri.TryCreate(uri, UriKind.Absolute, out created))
            {
                return this.addProtocol(absoluteName, created, uri);
            }

            if (Uri.TryCreate(uri, UriKind.Relative, out created))
            {
                return this.ConcatWithDomain(created, absoluteName);
            }

            return null;
        }

        /// <summary>
        /// Add or change protocol (scheme) data
        /// </summary>
        /// <param name="uri">URI to add protocol</param>
        /// <returns>Formatted URI</returns>
        private Uri addProtocol(Uri hostSitename, Uri uriPath, string uriStringPath)
        {
            if (uriStringPath.IndexOf(uriPath.Scheme) != -1)
                return uriPath;

            UriBuilder builder = new UriBuilder(uriPath)
            {
                Scheme = hostSitename.Scheme
            };

            return builder.Uri;
        }

        private Uri ConcatWithDomain(Uri BaseUri, Uri Domain)
        {
            return new Uri(Domain, BaseUri);
        }
    }
}