﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace CssFilesCollector
{
    class Program
    {
        static void Main(string[] args)
        {
            System.IO.File.AppendAllLines(@"D:\Applications\times.txt", new List<string>() { DateTime.Now.ToString("G", CultureInfo.CurrentCulture) });

            UrisFinder finder = new UrisFinder(10000);
            List<Uri> uris = finder.GetUris(SitesList.List).ToList();

            AsyncDownloader downloader = new AsyncDownloader(500, false);
            LinkExtractor extractor = new LinkExtractor();
            FilesSaver saver = new FilesSaver(@"D:\Applications\Temp", 4);

            foreach (var downloadPage in downloader.DownloadPages(uris))
            {
                foreach (var resultCssUri in extractor.GetLinks(downloadPage.Key, downloadPage.Value))
                {
                    Console.WriteLine("[{0}] css-file: {1}", DateTime.Now.ToString("G", CultureInfo.CurrentCulture), resultCssUri.Value);
                    
                    saver.DownloadAndSaveFileAsync(resultCssUri);
                }

                foreach (var resultCss in extractor.GetCssFromPage(downloadPage.Key, downloadPage.Value))
                {
                    Console.WriteLine("[{0}] css from page: {1}", DateTime.Now.ToString("G", CultureInfo.CurrentCulture), resultCss.Key);

                    saver.SaveToFileAsync(resultCss);
                }
            }

            try
            {
                ZipArchiveCreator archivator = new ZipArchiveCreator();
                archivator.Zip(@"D:\Applications\Temp", @"D:\Applications\TempCSS files.zip");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            System.IO.File.AppendAllLines(@"D:\Applications\times.txt", new List<string>() { DateTime.Now.ToString("G", CultureInfo.CurrentCulture) });
        }
    }
}