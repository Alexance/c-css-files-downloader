﻿using System;
using System.IO;
using System.IO.Compression;

namespace CssFilesCollector
{
    public class ZipArchiveCreator
    {
        public ZipArchiveCreator()
        {

        }

        /// <summary>
        /// Creates zip-archive from <paramref name="From"/> to 
        /// </summary>
        /// <param name="SourceDirectory">SourcePath</param>
        /// <param name="DestinationPath">Path for new archive</param>
        /// <param name="Level">Required level of compression</param>
        public void Zip(string SourceDirectory, string DestinationPath, CompressionLevel Level = CompressionLevel.NoCompression)
        {
            try
            {
                ZipFile.CreateFromDirectory(SourceDirectory, DestinationPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
